package controllers

import play.api._
import play.api.mvc._
import banner.eval.BANNER
import org.apache.commons.configuration.HierarchicalConfiguration
import org.apache.commons.configuration.XMLConfiguration
import play.Logger
import banner.tagging.CRFTagger
import java.io.File
import banner.types.Sentence
import scala.collection.JavaConversions._
import play.api.data.Form
import play.api._
import play.api.mvc._
import views._
import models._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.libs.json.Writes
import play.api.libs.json.JsValue
import play.api.libs.json.JsObject
import play.api.libs.json.Json
import play.api.libs.json.JsArray
import banner.types.Mention

case class Text(text: String)

object Application extends Controller {

  val confPath = Play.current.configuration.getString("chemdner.confg").getOrElse("")

  /**
   * Create BANNER-CHEMDNER config
   */
  val conf = new XMLConfiguration(confPath)

  /**
   * Create resources by calling BANNER helpers
   */
  val tokenizer = BANNER.getTokenizer(conf)
  val dictionary = BANNER.getDictionary(conf)
  val lemmatiser = BANNER.getLemmatiser(conf)
  val posTagger = BANNER.getPosTagger(conf)
  val brownClusters = BANNER.getBrownClusters(conf)
  val wordEmbeddings = BANNER.getWordEmbeddings(conf)
  val wordVectorClasses = BANNER.getWordVectorClasses(conf)
  val postProcessor = BANNER.getPostProcessor(conf)
  val localConfig = conf.configurationAt(classOf[BANNER].getPackage().getName)
  /**
   * Loading model file
   */
  val modelFilename = localConfig.getString("modelFilename");
  Logger.info("Model file: " + modelFilename)
  /**
   * Create tagger object
   */
  val tagger = CRFTagger.load(new File(modelFilename), lemmatiser, posTagger, dictionary, brownClusters, wordEmbeddings, wordVectorClasses)
  val plh = """Presynaptic mGlu7 receptors control GABA release in mouse hippocampus. The functional role of presynaptic release-regulating metabotropic glutamate type 7 (mGlu7) receptors in hippocampal GABAergic terminals was investigated. Mouse hippocampal synaptosomes were preloaded with [(3)H]D-γ-aminobutyric acid ([(3)H]GABA) and then exposed in superfusion to 12 mM KCl. The K(+)-evoked [(3)H]GABA release was inhibited by the mGlu7 allosteric agonist N,N'-dibenzyhydryl-ethane-1,2-diamine dihydrochloride (AMN082, 0.001-10 μM), as well as by the group III mGlu receptor agonist l-(+)-2-amino-4-phosphonobutyric acid [(l)-AP4, 0.01-1 mM]. The mGlu8 receptor agonist (S)-3,4-dicarboxyphenylglycine [(S)-3,4-DCPG, 10-100 nM] was ineffective. AMN082 and (l)-AP4-induced effects were recovered by the mGlu7 negative allosteric modulator (NAM) 6-(4-methoxyphenyl)-5-methyl-3-(4-pyridinyl)-isoxazolo[4,5-c]pyridin-4(5H)-one hydrochloride (MMPIP). AMN082 also inhibited in a MMPIP-sensitive manner the K(+)-evoked release of endogenous GABA. AMN082 and the adenylyl cyclase (AC) inhibitor MDL-12,330A reduced [(3)H]GABA exocytosis in a 8-Br-cAMP-sensitive. AMN082-inhibitory effect was additive to that caused by (-)baclofen, but insensitive to the GABA(B) antagonist 3-[[(3,4-Dichlorophenyl)methyl]amino]propyl] diethoxymethyl) phosphinic acid (CGP52432). Conversely, (-)baclofen-induced inhibition of GABA exocytosis was insensitive to MMPIP. Finally, the forskolin-evoked [(3)H]GABA release was reduced by AMN082 or (-)baclofen but abolished when the two agonists were added concomitantly. Mouse hippocampal synaptosomal plasmamembranes posses mGlu7 receptor proteins; confocal microscopy analysis unveiled that mGlu7 proteins colocalize with syntaxin-1A (Stx-1A), with vesicular GABA transporter (VGAT)-proteins and with GABA(B) receptor subunit proteins. We propose that presynaptic inhibitory mGlu7 heteroreceptors, negatively coupled to AC-dependent intraterminal pathway, exist in mouse hippocampal GABA-containing terminals, where they colocalize, but do not functionally cross-talk, with GABA(B) autoreceptors. This article is part of a Special Issue entitled 'Metabotropic Glutamate Receptors'."""
  val textForm = Form(
    mapping(
      "text" -> default(nonEmptyText, plh))((text) => Text(text))((text: Text) => Some(text.text)))

  /**
   * Sample sentences 
   */
  val s1 = """Effectiveness of preoperative plasmapheresis in a pregnancy complicated by hyperthyroidism and anti-thyroid drug-associated angioedema.	Abstract Hyperthyroidism is not a rare entity in pregnancy and 85% of these cases attributed to Graves' disease (GD). There is no therapeutic modality for GD considered as totally safe in pregnancy. Fetal and neonatal risks of maternal hyperthyroid disease are related to the hyperthyroidism itself and/or to the medical treatment of the disease. There are no data supporting an association between congenital anomalies in the fetus and propylthiouracil (PTU). Hepatotoxicity, cytopenias - especially agranulocytosis and quite rarely, angioedema, may be seen as side effects of PTU. In this case report, we examine an instance of Graves' hyperthyroidism diagnosed during pregnancy. In this case, a serious side effect during anti-thyroid drug usage was encountered, eventually resulting in surgery in the second trimester. This intervention was assisted by the use of plasmapheresis to obtain rapid normalization of serum thyroid hormone levels."""
  val s2 = """Potential use of polymeric nanoparticles for drug delivery across the blood-brain barrier.	Nanomedicine is certainly one of the scientific and technological challenges of the coming years. In particular, biodegradable nanoparticles formulated from poly (D,L-lactide-co-glycolide) (PLGA) have been extensively investigated for sustained and targeted delivery of different agents, including recombinant proteins, plasmid DNA, and low molecular weight compounds. PLGA NPs present some very attractive properties such as biodegradability and biocompatibility, protection of drug from degradation, possibility of sustained release, and the possibility to modify surface properties to target nanoparticles to specific organs or cells. Moreover, PLGA NPs have received the FDA and European Medicine Agency approval in drug delivery systems for parenteral administration, thus reducing the time for human clinical applications. This review in particular deals on surface modification of PLGA NPs and their possibility of clinical applications, including treatment for brain pathologies such as brain tumors and Lysosomal Storage Disorders with neurological involvement. Since a great number of pharmacologically active molecules are not able to cross the Blood-Brain Barrier (BBB) and reach the Central Nervous System (CNS), new brain targeted polymeric PLGA NPs modified with glycopeptides (g7- NPs) have been recently produced. In this review several in vivo biodistribution studies and pharmacological proof-of evidence of brain delivery of model drugs are reported, demonstrating the ability of g7-NPs to create BBB interaction and trigger an efficacious BBB crossing. Moreover, another relevant development of NPs surface engineering was achieved by conjugating to the surface of g7-NPs, some specific and selective antibodies to drive NPs directly to a specific cell type once inside the CNS parenchyma."""
  val s3 = """QM/MM simulations of vibrational spectra of bacteriorhodopsin and channelrhodopsin-2.	Channelrhodopsin-2 is a light-gated ion channel, which has been studied intensively over the last decade. Vibrational spectroscopic experiments started to shed light on the structural changes, that occur during the photocycle, especially in the hydrogen-bonded network surrounding the protonated D156 and C128 - the DC gate. However, the interpretation of these experiments was only based on homology models. Since then, an X-ray structure and better computational models became available. In this article, we show that in combination with a recent reparametrization, the approximate DFT method, DFTB, is able to describe the effects of hydrogen bonding on the C[double bond, length as m-dash]O stretch vibration in carboxylic acids reliably and agrees well with full DFT results. We apply DFTB in a QM/MM framework to perform vibrational analysis of buried aspartic acids in bacteriorhodopsin and channelrhodopsin-2. Using this approach, we can simulate the FTIR spectral difference between D115 in the dark-adapted and K states of bacteriorhodopsin. The FTIR experiments on the DC gate in channelrhodopsin-2 are well described using an indirect model, where D156 and C128 are bridged via a water molecule."""
  val s4 = """Toxicological and mutagenic analysis of Artemisia dracunculus (tarragon) extract.	Mutagenicity and liver toxicity of the herb tarragon (Artemisia dracunculus) were evaluated using single cell gel (comet) electrophoresis. Ten microlitres aliquots of peripheral venous human blood were incubated with tarragon extract, saline, or the mutagen sodium dichromate. Cell suspensions dispersed in low-melting agarose were electrophoresed in ethidium bromide. The resulting DNA migration trails were obtained using fluorescent microscopy at 400× magnification, and graded according to the mutagenicity index (MI) for each cell incubation condition. The in vivo liver toxicity of Artemisia dracunculus was assessed in the blood of mice treated orally with the extract of the herb, using alanine aminotransferase (ALT) and aspartate aminotransferase (AST) as liver function indicators. Liver morphology was assessed using hematoxylin and eosin (HE) staining of liver tissue. The present study demonstrated a direct correlation between tarragon extract dosage and three major outcome variables: MI; serum liver enzyme activity; and liver histopathology. These outcomes are possibly due to the presence in tarragon of methylchavicol and other genotoxic compounds. These findings provide a preliminary guide for risk assessment of tarragon in diet and in possible therapeutic applications."""
  val s5 = """Do the physical properties of water in mixed reverse micelles follow a synergistic effect: a spectroscopic investigation.	In this contribution we have tried to investigate whether the mechanical properties of the reverse micellar (RM) interface dictate the physical properties of entrapped water molecules in the RM waterpool. We choose AOT/Igepal-520/cyclohexane (Cy) mixed RM as a model system which exhibits synergistic water solubilization behavior as a function of interfacial stoichiometry. Such a phenomenon associates systematic modification of the interface curvature. Dynamic light scattering (DLS) studies reveal linear increase in the droplet size and aggregation number of the RMs with increasing XIgepal (mole fraction of Igepal in the surfactant mixture). FTIR study in the 3000-3800 cm(-1) region identifies that the relative population of the surface-bound water molecules is higher in AOT RM compared to that in Igepal RM, and in mixed systems it also follows a linear trend with XIgepal. Water relaxation dynamics as probed by time-resolved fluorescence spectroscopy using Coumarin-500 also reveals an overall linear trend with no characteristic feature around the solubilization inflation point. Our study clearly identifies that the physical properties of water in RM are mostly governed by the interfacial stoichiometry and water content, and merely bares any dependence on the mechanical properties of the interface."""

  val ss = List(plh, s1, s2, s3, s4, s5)

  def index = Action {
    Ok(views.html.textForm(textForm, plh, ss))
  }

  def tagForm() = Action { implicit request =>
    textForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.textForm(formWithErrors, plh, ss)),
      text => {
        val sentence = new Sentence("1", "", text.text)
        /**
         * Identifying mentions
         */
        val taggedText = BANNER.process(tagger, tokenizer, postProcessor, sentence)
        val mentions = taggedText.getMentions().toList
        var markedText = ""
        var lastEnd = 0
        mentions.foreach { mention =>
          val start = mention.getStartChar()
          val end = mention.getEndChar()
          markedText = markedText + text.text.substring(lastEnd, start) + "<mark>" + text.text.substring(start, end) + "</mark>"
          lastEnd = end
          // markedText = markedText.substring(0, start) + "<mark>" + markedText.substring(start, end) + "</mark> " + markedText.substring(end, markedText.length)
          // c = c + 1
        }
        markedText = markedText + text.text.substring(lastEnd, text.text.length)
        Ok(views.html.tag(mentions, markedText))
      })
  }

  def tag(text: String) = Action {
    val sentence = new Sentence("1", "", text)
    val taggedText = BANNER.process(tagger, tokenizer, postProcessor, sentence)
    val mentions = taggedText.getMentions().toList
    Ok(Json.toJson(mentions)(MentionsToJson))
  }

  implicit object MentionsToJson extends Writes[List[Mention]] {
    def writes(as: List[Mention]): JsValue = JsArray(
      as.map(a => Json.toJson(a)(MentionToJson)).toList)
  }

  implicit object MentionToJson extends Writes[Mention] {
    def writes(a: Mention): JsValue = Json.obj(
      "mention" -> Json.toJson(a.getText()),
      "start" -> Json.toJson(a.getStartChar()),
      "end" -> Json.toJson(a.getEndChar()))
  }

}